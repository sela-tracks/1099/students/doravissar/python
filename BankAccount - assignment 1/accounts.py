from Bank2002 import bank_account
dor_bank = bank_account(name="dor avissar", id="123", balance=600)
ori_bank = bank_account(name="ori david", id="1234", balance=50)
bank_accounts = {"123": dor_bank, "1234": ori_bank}
selected_account = None

def create_account():
    while True:
        account_name = input("Enter full name: ")
        account_id = input("Enter ID: ")
        try:
            account_balance = float(input("Enter balance: "))
        except ValueError:
            print("Invalid input! Balance should be a number.")
            continue
        if account_id in bank_accounts: # Check if ID already exists
            print("An account with this ID already exists. Please try again.")
            continue
        if not account_name.replace(" ", "").isalpha():
            print("Invalid input! Name should only contain alphabetic characters.")
            continue
        if not account_id.isdigit() or len(account_id)!=9:
            print("Invalid input! ID should only contain 9 digits")
            continue
        # If all checks pass, create the account and break the loop
        new_account = bank_account(name=account_name, id=account_id, balance=account_balance)
        bank_accounts[account_id] = new_account
        break

def select_account():
    global selected_account
    input_id = input("Enter your ID: ")
    if not  input_id.isdigit():
        print("Invalid input! ID should only contain digits.")
    else:
        selected_account = bank_accounts.get(input_id,None)
        if selected_account is None:
            print("no such account, please try again")


account_created_or_selected = False

while True:
    action = input("Enter a number:\n"
                   "1 = Create account\n"
                   "2 = Select account\n"
                   "3 = Deposit\n"
                   "4 = Withdrawal\n"
                   "5 = Account History\n"
                   "6 = Transfer Money\n"
                   "7 = show balance\n"
                   "8 = Exit\n"
                   "Enter your choice: ")

    if action == "1":
        create_account()
        account_created_or_selected = True
    elif action == "2":
        select_account()
        account_created_or_selected = True
    elif action in ["3", "4", "5", "6", "7"]:
        if not account_created_or_selected:
            print("You need to create or select an account first.")
            continue
        if action == "3":
            amount1 = input("Enter how much you wish to deposit: ")
            selected_account.Deposit(amount1)
        elif action == "4":
            amount2 = input("Enter how much you wish to withdraw: ")
            selected_account.withdrawal(amount2)
        elif action == "5":
            print(selected_account.get_history())
        elif action == "6":
            amount3 = input("Enter how much you wish to transfer: ")
            destination = input("Enter who you want to make a money transfer to: ")
            dest = bank_accounts.get(destination, None)
            if dest is not None:
                selected_account.transfer(amount3, dest)
        elif action == "7":
            print(selected_account.get_balance())
    elif action == "8":
        print("Exiting...")
        break
    else:
        print("Invalid input. Please enter a number between 1-8")





