import json
import datetime
class bank_account:
    def __init__(self, name: str, id:str, balance:float):
        self.name = name
        self.id = id
        self.balance = float(balance)
        self.commission = 0.1
        self.history = []

    def Deposit(self, amount:float):
        self.balance = self.balance + amount * (1-self.commission)
        current_time = datetime.datetime.now()
        (self.history.append
            ({
            "action": "deposit",
            "time": current_time.strftime("%Y-%m-%d %H:%M"),
            "amount": amount
        }))
        self.update_json_balance()
    def withdrawal(self, amount:str):
        if amount.isdigit():
            amount=float(amount)
            if self.balance >= amount * (1 + self.commission):
                self.balance -= amount * (1 + self.commission)
                current_time = datetime.datetime.now()
                (self.history.append
                    ({
                        "action": "withdrawal",
                        "time": current_time.strftime("%Y-%m-%d %H:%M"),
                        "amount": amount
                }))
                return True
            else:
                print("The operation failed")
                return False
        else:
            print("The operation failed")
            return False
        self.update_json_balance()

    def transfer(self, amount: str, destination_account):
        a = self.withdrawal(amount)
        if a:
            amount = float(amount)
            destination_account.Deposit(amount)
        else:
            print("Fail to transfer")

    def get_history(self):
        return self.history

    def get_balance(self):
        print("your balance is: ")
        return self.balance

    def update_json_balance(self):
        try:
            with open("customers.json", "r+") as json_file:
                accounts_data = json.load(json_file)
                accounts_data[self.id]["balance"] = self.balance
                json_file.seek(0)
                json.dump(accounts_data, json_file, indent=4)
                json_file.truncate()
        except FileNotFoundError:
            print("Error: JSON file not found.")



